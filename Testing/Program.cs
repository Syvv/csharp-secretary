﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testing
{
    public class Program
    {
        private static int failures = 0;
        static void Main(string[] args)
        {
            //Run tests
            FileHandlerTest.TryCheckingIfDirectoryExists();
            FileHandlerTest.TryWritingAndReadingObjects();
            FileHandlerTest.TryReadingLines();
            FileHandlerTest.TryDeletingFilesAndDirectories();
            FileHandlerTest.TryWritingText();

            LoggerTest.TryLoggingAMessage();
            
            //Report amount of failures
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Tests finsished with "+failures+" failure(s).");

            //Reset state
            Console.WriteLine("Resetting state ...");
            FileHandlerTest.Reset();
            Console.WriteLine("State has been reset, press any key to exit.");

            //Persist window
            Console.ReadKey();
        }

        public static void Success(string message)
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("[" + DateTime.Now.ToLongTimeString() + "] PASSED -> " + message);
        }

        public static void Failure(string message)
        {
            failures++;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("[" + DateTime.Now.ToLongTimeString() + "] FAILED -> " + message);
        }

        public static void Error(string message)
        {
            failures++; //Errors count as failures
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("[" + DateTime.Now.ToLongTimeString() + "] EXCEPTION -> " + message);
        }
    }
}
