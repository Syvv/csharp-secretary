﻿using Secretary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testing
{
    class FileHandlerTest
    {
        public static void TryWritingAndReadingObjects()
        {
            //Testing reading an object
            try
            {
                Example temp = FileHandler.Read<Example>("../../testFiles/ReadingObjectTest.bin");
                if (temp.Name == "Dreamcatcher")
                {
                    Program.Success("FileHandler.Read can read back a saved object");
                }
                else
                {
                    Program.Failure("FileHandler.Read can read back a saved object");
                }
            }
            catch (Exception e)
            {
                Program.Error(e.Message);
            }
            //Writing an object to a file
            Example example = new Example("");
            try
            {
                example = new Example("Red Velvet");
                FileHandler.Write(example, "../../tmp/redvelvet.bin", true); //Mention true explicitly in case default gets changed
            }
            catch (Exception e)
            {
                Program.Error(e.Message);
            }
            //Checking if the file exists
            try
            {
                if (FileHandler.FileExists("../../tmp/redvelvet.bin"))
                {
                    Program.Success("FileHandler.FileExists correctly finds redvelvet.bin");
                }
                else
                {
                    Program.Failure("FileHandler.FileExists correctly finds redvelvet.bin");
                }
            }
            catch (Exception e)
            {
                Program.Error(e.Message);
            }
            //Reading the object from the file
            try
            {
                Example temp = FileHandler.Read<Example>("../../tmp/redvelvet.bin");
                if (temp.Name == example.Name)
                {
                    Program.Success("FileHandler.Write writes away the object correctly so that it can be read back again");
                }
                else
                {
                    Program.Failure("FileHandler.Write writes away the object correctly so that it can be read back again");
                }
            }
            catch (Exception e)
            {
                Program.Error(e.Message);
            }
        }

        public static void TryReadingLines()
        {
            //Reading the lines from the file
            try
            {
                string[] lines = FileHandler.Read("../../testFiles/lines.txt");
                if (lines.Length == 29)
                {
                    Program.Success("FileHandler.Read reads "+ lines.Length +" of 29 lines from lines.txt");
                }
                else
                {
                    Program.Failure("FileHandler.Read reads " + lines.Length + " of 29 lines from lines.txt");
                }
                if (lines[0] == "잔소리는 stop it 알아서 할게")
                {
                    Program.Success("FileHandler.Read reads " + lines[0] + " as the first line from lines.txt");
                }
                else
                {
                    Program.Failure("FileHandler.Read reads " + lines[0] + " as the first line from lines.txt");
                }
            }
            catch (Exception e)
            {
                Program.Error(e.Message);
            }
        }

        public static void TryCheckingIfDirectoryExists()
        {
            //Checking if a directory exists
            try
            {
                if (FileHandler.DirectoryExists("../../testFiles"))
                {
                    Program.Success("FileHandler.DirectoryExists sees the directory testFiles");
                }
                else
                {
                    Program.Failure("FileHandler.DirectoryExists sees the directory testFiles");
                }
            }
            catch (Exception e)
            {
                Program.Error(e.Message);
            }
        }

        public static void TryDeletingFilesAndDirectories()
        {
            //Creating a directory
            try
            {
                FileHandler.CreateDirectory("../../tmp/NewFolder");
                if (FileHandler.DirectoryExists("../../tmp/NewFolder"))
                {
                    Program.Success("FileHandler.CreateDirectory creates the directory NewFolder correctly.");
                }
                else
                {
                    Program.Failure("FileHandler.CreateDirectory creates the directory NewFolder correctly.");
                }
            }
            catch (Exception e)
            {
                Program.Error(e.Message);
            }

            //Deleting a directory
            try
            {
                bool deleted = FileHandler.DeleteDirectory("../../testFiles/test", true);
                if (deleted && !FileHandler.DirectoryExists("../../testFiles/test"))
                {
                    Program.Success("FileHandler.DeleteDirectory deletes the directory test correctly.");
                }
                else
                {
                    Program.Failure("FileHandler.DeleteDirectory deletes the directory test correctly.");
                }
            }
            catch (Exception e)
            {
                Program.Error(e.Message);
            }

            //Deleting files in a directory
            try
            {
                FileHandler.DeleteAllFiles("../../tmp", ".bin");
                if (!FileHandler.FileExists("../../tmp/redvelvet.bin"))
                {
                    Program.Success("FileHandler.DeleteAllFiles deletes the .bin files in the directory tmp correctly.");
                }
                else
                {
                    Program.Failure("FileHandler.DeleteAllFiles deletes the .bin files in the directory tmp correctly.");
                }
            }
            catch (Exception e)
            {
                Program.Error(e.Message);
            }
        }

        public static void TryWritingText()
        {
            //Writing text
            try
            {
                FileHandler.WriteText("apple1\napple2\napple3", "../../tmp/txts/apples.txt", WriteModes.CREATE);
                if (FileHandler.Read("../../tmp/txts/apples.txt")[0] == "apple1")
                {
                    Program.Success("FileHandler.WriteText writes correctly with WriteMode CREATE.");
                }
                else
                {
                    Program.Failure("FileHandler.WriteText writes correctly with WriteMode CREATE.");
                }
            }
            catch (Exception e)
            {
                Program.Error(e.Message);
            }
            //Writing text
            try
            {
                FileHandler.WriteText("apple1\napple2\napple3", "../../tmp/txts/apples.txt", WriteModes.CREATE_OR_APPEND);
                if (FileHandler.Read("../../tmp/txts/apples.txt")[5] == "apple3")
                {
                    Program.Success("FileHandler.WriteText writes correctly with WriteMode CREATE_OR_APPEND.");
                }
                else
                {
                    Program.Failure("FileHandler.WriteText writes correctly with WriteMode CREATE_OR_APPEND.");
                }
            }
            catch (Exception e)
            {
                Program.Error(e.Message);
            }
            //Writing text
            try
            {
                FileHandler.WriteText("apple1\napple2\napple3", "../../tmp/txts/apples.txt", WriteModes.CREATE_OR_WRITE);
                if (FileHandler.Read("../../tmp/txts/apples.txt")[0] == "apple1" && FileHandler.Read("../../tmp/txts/apples.txt").Length == 3)
                {
                    Program.Success("FileHandler.WriteText writes correctly with WriteMode CREATE_OR_WRITE.");
                }
                else
                {
                    Program.Failure("FileHandler.WriteText writes correctly with WriteMode CREATE_OR_WRITE.");
                }
            }
            catch (Exception e)
            {
                Program.Error(e.Message);
            }
            //Writing text
            try
            {
                FileHandler.WriteText("apple1\napple2\napple3", "../../tmp/txts/apples.txt", WriteModes.APPEND);
                if (FileHandler.Read("../../tmp/txts/apples.txt")[5] == "apple3")
                {
                    Program.Success("FileHandler.WriteText writes correctly with WriteMode APPEND.");
                }
                else
                {
                    Program.Failure("FileHandler.WriteText writes correctly with WriteMode APPEND.");
                }
            }
            catch (Exception e)
            {
                Program.Error(e.Message);
            }
            //Writing text with array parameter
            try
            {
                string[] array = new string[] { "apple1", "apple2", "apple3" };
                FileHandler.WriteText(array, "../../tmp/txts/apples.txt", WriteModes.APPEND);
                if (FileHandler.Read("../../tmp/txts/apples.txt")[8] == "apple3")
                {
                    Program.Success("FileHandler.WriteText writes correctly with WriteMode APPEND when given an array.");
                }
                else
                {
                    Program.Failure("FileHandler.WriteText writes correctly with WriteMode APPEND when given an array.");
                }
            }
            catch (Exception e)
            {
                Program.Error(e.Message);
            }
        }

        public static void Reset()
        {
            FileHandler.CreateDirectory("../../testFiles/test");
            FileHandler.WriteText("Chorong", "../../testFiles/test/Apink.txt", WriteModes.CREATE);
            FileHandler.DeleteDirectory("../../tmp", true);
        }
    }
}
