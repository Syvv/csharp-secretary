﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testing
{
    [Serializable]
    class Example
    {
        public string Name { get; set; }

        public Example(string name)
        {
            Name = name;
        }
    }
}
