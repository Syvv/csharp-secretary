﻿using Secretary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testing
{
    public class LoggerTest
    {
        public static void TryLoggingAMessage()
        {
            Logger logger = new Logger("../../log.txt");
            try
            {
                if (logger.Log("드림캐쳐", LogLevel.INFO))
                {
                    Program.Success("Log a message");
                }else
                {
                    Program.Failure("Log a message");
                }
            }catch (Exception e)
            {
                Program.Error(e.Message);
            }
        }
    }
}
