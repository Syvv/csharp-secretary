﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secretary
{
    /// <summary>
    /// The mode in which you want to write to a file.
    /// </summary>
    public enum WriteModes
    {
        /// <summary>
        /// Only writes if the file doesn't exist.
        /// </summary>
        CREATE,
        /// <summary>
        /// Either creates if the file doesn't exist or appends if it does exist.
        /// </summary>
        CREATE_OR_APPEND,
        /// <summary>
        /// Either creates if the file doesn't exist yet or overwrites the current content if it does exist.
        /// </summary>
        CREATE_OR_WRITE,
        /// <summary>
        /// Adds to the end of the file.
        /// </summary>
        APPEND,
    }
}
