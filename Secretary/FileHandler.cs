﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Secretary
{ 
    /// <summary>
    /// Handles all file interactions for you.
    /// </summary>
    public class FileHandler
    {
        /// <summary>
        /// Reads an object from a file and deserializes it to the object type specified.
        /// </summary>
        /// <typeparam name="T">The type of object that is saved in the file that is trying to be read.</typeparam>
        /// <param name="path">The path of the file.</param>
        /// <returns>The object that is in the file, deserialized.</returns>
        public static T Read<T>(string path)
        {
            using (FileStream reader = new FileStream(path, FileMode.Open))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                return (T)formatter.Deserialize(reader);
            };
        }

        /// <summary>
        /// Reads all lines from a file and returns them as a list.
        /// </summary>
        /// <param name="path">The path of the file.</param>
        /// <returns>All lines in the specified file.</returns>
        public static string[] Read(string path)
        {
            List<string> lines = new List<string>();
            string line;
            StreamReader file = new StreamReader(path);
            while ((line = file.ReadLine()) != null)
            {
                lines.Add(line);
            }
            file.Close();
            return lines.ToArray();
        }

        /// <summary>
        /// Writes an object to a file.
        /// </summary>
        /// <param name="obj">The <c>[Serializable]</c> object to write.</param>
        /// <param name="path">The path to write the object to.</param>
        /// <param name="createDirectories">If you want to create the directories required if they don't exist yet.</param>
        /// <returns>If the write operation has succeeded.</returns>
        public static bool Write(object obj, string path, bool createDirectories = true)
        {
            try
            {
                using (FileStream writer = new FileStream(path, FileMode.Create))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(writer, obj);
                };
                return true;
            }
            catch (Exception e)
            {
                if (e is DirectoryNotFoundException && createDirectories)
                {
                    string directoryPath = Path.GetDirectoryName(path);
                    CreateDirectory(directoryPath);
                    return Write(obj, path, false); //If for some reason the directory didn't get created we don't want to create an infinite loop by trying again and again.
                }
                if (!(e is IOException || e is SerializationException))
                {
                    throw e;
                }
                return false; //Non breaking exceptions are given back as a failure bool so you can handle it yourself
            }
        }

        /// <summary>
        /// Checks if a file exists and is not empty.
        /// </summary>
        /// <param name="path">The path of the file.</param>
        /// <returns>If the file exists and contains data.</returns>
        public static bool FileExists(string path)
        {
            return File.Exists(path) && new FileInfo(path).Length != 0;
        }

        /// <summary>
        /// Checks if a directory exists.
        /// </summary>
        /// <param name="path">The path of the directory.</param>
        /// <returns>If the directory exists.</returns>
        public static bool DirectoryExists(string path)
        {
            try
            {
                Directory.Exists(path);
            }catch (Exception)
            {
                return false;
            }
            return Directory.Exists(path);
        }

        /// <summary>
        /// Writes text to a file according to the chosen mode.
        /// </summary>
        /// <param name="text">Text to write.</param>
        /// <param name="path">Path of the file.</param>
        /// <param name="writeMode">The mode in which to write text</param>
        /// <returns>If the text has been written to the file successfully</returns>
        public static bool WriteText(string text, string path, WriteModes writeMode)
        {
            string directoryPath = Path.GetDirectoryName(path);
            if (!DirectoryExists(directoryPath))
            {
                CreateDirectory(directoryPath);
            }

            switch(writeMode)
            {
                case WriteModes.APPEND:
                    if(FileExists(path))
                    {
                        using (StreamWriter streamWriter = new StreamWriter(path, true))
                        {
                            streamWriter.WriteLine(text);
                        };
                    }
                    break;
                case WriteModes.CREATE:
                    if (!FileExists(path))
                    {
                        using (StreamWriter streamWriter = new StreamWriter(File.Create(path)))
                        {
                            streamWriter.WriteLine(text);
                        };
                    }
                    break;
                case WriteModes.CREATE_OR_APPEND:
                    if (!FileExists(path))
                    {
                        using (StreamWriter streamWriter = new StreamWriter(File.Create(path)))
                        {
                            streamWriter.WriteLine(text);
                        };
                    }
                    else
                    {
                        using (StreamWriter streamWriter = new StreamWriter(path, true))
                        {
                            streamWriter.WriteLine(text);
                        };
                    }
                    break;
                case WriteModes.CREATE_OR_WRITE:
                    if (!FileExists(path))
                    {
                        using (StreamWriter streamWriter = new StreamWriter(File.Create(path)))
                        {
                            streamWriter.WriteLine(text);
                        };
                    }
                    else
                    {
                        using (StreamWriter streamWriter = new StreamWriter(path, false))
                        {
                            streamWriter.WriteLine(text);
                        };
                    }
                    break;
            }
            return false;
        }

        /// <summary>
        /// Writes text lines to a file according to the chosen mode.
        /// </summary>
        /// <param name="lines">Text lines to write.</param>
        /// <param name="path">Path of the file.</param>
        /// <param name="writeMode">The mode in which to write text</param>
        /// <returns>If the text has been written to the file successfully</returns>
        public static bool WriteText(string[] lines, string path, WriteModes writeMode)
        {
            StringBuilder text = new StringBuilder();
            foreach(string line in lines)
            {
                text.Append(line + "\n");
            }
            return WriteText(text.ToString(), path, writeMode);
        }

        /// <summary>
        /// Deletes a file.
        /// </summary>
        /// <param name="path">The file to delete.</param>
        public static void DeleteFile(string path)
        {
            FileInfo file = new FileInfo(path)
            {
                Attributes = FileAttributes.Normal
            };
            File.Delete(path);
        }

        /// <summary>
        /// Deletes all files of a certain type from the directory. 
        /// </summary>
        /// <param name="pathOfDirectory">The directory to delete files from.</param>
        /// <param name="fileExtension">The exstension of files to delete. If not set when calling this method it will delete all files regardless of extension.</param>
        /// <example>
        /// <code>
        /// string path = "src/tmp";
        /// if(FileHandler.DirectoryExists(path))
        /// {
        ///     //Deletes all .bin files from the directory src/tmp
        ///     FileHandler.DeleteAllFiles(path, ".bin") ;
        ///     
        ///     //Deletes all files from the directory src/tmp/garbage
        ///     FileHandler.DeleteAllFiles(path + "/garbage");
        /// }
        /// </code>
        /// </example>
        public static void DeleteAllFiles(string pathOfDirectory, string fileExtension = ".*")
        {
            //Get all files in the directory
            DirectoryInfo di = new DirectoryInfo(pathOfDirectory);
            FileInfo[] files = di.GetFiles("*"+fileExtension);
            //Delete all files
            foreach (FileInfo file in files)
            {
                file.Attributes = FileAttributes.Normal;
                File.Delete(file.FullName);
            }
        }

        /// <summary>
        /// Deletes an entire directory and all files in it.
        /// </summary>
        /// <param name="path">The directory to delete.</param>
        /// <param name="cascade">If set to true also deletes all directories that are in the specified directory.</param>
        /// <remarks>If cascade is not set to true (false by default) and there are directories in the specified directory the method will return false and the directory will not be deleted, the files will be deleted though.</remarks>
        /// <returns>If the directory has been deleted.</returns>
        public static bool DeleteDirectory(string path, bool cascade = false)
        {
            if(!DirectoryExists(path))
            {
                return false;
            }
            //Delete all subdirectories if cascading is selected (Also cascades through all of their subdirectories)
            if (cascade)
            {
                string[] subdirectories = Directory.GetDirectories(path);
                foreach(string directoryPath in subdirectories)
                {
                    DeleteDirectory(directoryPath, true);
                }
            }
            DeleteAllFiles(path);
            //Delete the directory itself
            try
            {
                Directory.Delete(path);
                return true;
            }catch (IOException e)
            {
                if(e.Message == "The directory is not empty.\r\n")
                {
                    return false;
                }
                throw e;
            }
        }

        /// <summary>
        /// Creates a directory and all its sub directories if they don't exist yet.
        /// </summary>
        /// <param name="path">The path of the directory to create</param>
        public static void CreateDirectory(string path)
        {
            Directory.CreateDirectory(path);
        }
    }
}
