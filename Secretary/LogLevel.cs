﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secretary
{
    /// <summary>
    /// The level of importance of a logged message.
    /// </summary>
    public enum LogLevel
    {
        /// <summary>
        /// Only for debugging purposes, won't be logged when not running in debug mode.
        /// </summary>
        DEBUG,
        /// <summary>
        /// Low level info.
        /// </summary>
        INFO,
        /// <summary>
        /// For messages you should do something with, but they won't break anything.
        /// </summary>
        WARNING,
        /// <summary>
        /// Something went wrong.
        /// </summary>
        ERROR
    }
}
