﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secretary
{
    /// <summary>
    /// Handles logging to a given log file.
    /// </summary>
    public class Logger
    {
        private string FilePath { get; set; }

        public Logger(string filePath)
        {
            FilePath = filePath;
        }

        /// <summary>
        /// Logs a message to the log file with the specified urgency.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="logLevel">The level of urgency of the message.</param>
        /// <returns>If the logging succeeded</returns>
        public bool Log(string message, LogLevel logLevel)
        {
            StringBuilder logBuilder = new StringBuilder("[");
            // Get the name of the enum
            logBuilder.Append(Enum.GetName(typeof(LogLevel), logLevel));
            // Add the time
            logBuilder.Append(" at " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + "] ");
            // finally add the message
            logBuilder.Append(message);

            try
            {
                if (logLevel == LogLevel.DEBUG)
                {
                    LogDebugMessage(logBuilder.ToString());
                    return true;
                }
                else
                {
                    LogMessage(logBuilder.ToString());
                    return true;
                }
            }catch (Exception)
            {
                // We don't want the logger to shut down the entire application so we fail silently.
                return false;
            }
        }

        [Conditional("DEBUG")]
        private void LogDebugMessage(string message)
        {
            FileHandler.WriteText(message, FilePath, WriteModes.CREATE_OR_APPEND);
        }

        private void LogMessage(string message)
        {
            FileHandler.WriteText(message, FilePath, WriteModes.CREATE_OR_APPEND);
        }
    }
}
