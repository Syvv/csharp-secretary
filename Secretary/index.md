
## C# Secretary.
C# Secretary is a simple file handling and logging wrapper designed to make easy tasks trivial.
###Latest versions
-	v0.2.0 (2020-8-19) [(download)](https://mega.nz/file/its3RBzJ#cqIihhocH6iPaiu29ANlYSn7f9D3XTPsqolhS1-FseU)

	*	Added a logging class
	*	Logger can log on multiple different levels
	*	Debug messages only get logged when running in debug mode

-	v0.1.1 (2020-4-16) [(download)](https://mega.nz/file/GtFHiYxb#RpJV-eC5_fTZvBgpbjC8omPeaminGI5dqZUdCzaT6H8)
	
	*	You can now write an array of strings to a file
	*	You can now choose in which way you want to write text to a file (Append, Create, Create or append, Create or write)
	*	You can now create directories
	*	Writing an object to a file can now create non existing directories if you want to
	*	Removed the requirement to import List by returning an array of strings for the method FileHandler.Read()

-	v0.1 (2020-4-13) [(download)](https://mega.nz/folder/S5EwyIKL#vAYOGtB7rZJ887Mw5ufqCw)

	*	Handling binairy files (read, write)
	*	Handling text files (read, write)
	*	Deleting files and directories